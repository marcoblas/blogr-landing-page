//obtener los botones
let dropdowns=document.querySelectorAll(".drpdown");
let mobileMenu=document.querySelector(".menu-container-mobile");
let checkbox=document.getElementById("checkbox")

//Desplegar menu
dropdowns.forEach((dd) => {
  dd.addEventListener("click", () => {
    let el = dd.nextElementSibling;    
    
    for(i=0;i<dropdowns.length;i++){
      if(dropdowns[i].nextElementSibling===el){
        el.classList.contains('show')?el.classList.remove('show'):el.classList.add('show');
        el.classList.contains('show')?dd.lastChild.previousElementSibling.style.transform="rotate(180deg)":dd.lastChild.previousElementSibling.style.transform="none"
      }else if(dropdowns[i].nextElementSibling.classList.contains('show')){
        dropdowns[i].nextElementSibling.classList.remove('show')
        dropdowns[i].lastChild.previousElementSibling.style.transform="none"
      }
    }
  });
});

checkbox.addEventListener("click",()=>{
  if(checkbox.checked===true){
    mobileMenu.style.display="flex";
  }else{
    mobileMenu.style.display="none"
  }
});